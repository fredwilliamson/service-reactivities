﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Commands;
using Application.Queries;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class AbstractV1Controller<T> : ControllerBase where T : IPrimaryKey
    {
        private IMediator _mediator;

        public AbstractV1Controller(IMediator mediator)
        {
            _mediator = mediator;
        }

        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();

        [HttpGet("{id}")]
        public virtual async Task<ActionResult<T>> GetById(Guid id)
        {
            var result = await Mediator.Send(new KeyQuery<T>{Id = id});
            return HandleQueryResult(result);
        }

        protected ActionResult<T> HandleQueryResult(IResultQuery<T> result)
        {
            if (result.IsSuccess && result.Value == null) return NotFound();

            if (!result.IsSuccess) return BadRequest(result.Message);
            return Ok(result.Value);
        }

        protected ActionResult<ICollection<T>> HandleQueryListResult(
            IResultQuery<ICollection<T>> result)
        {
            if (result.IsSuccess && (result.Value == null || !result.Value.Any())) return NotFound();

            if (!result.IsSuccess) return BadRequest(result.Message);
            return Ok(result.Value);
        }

        protected async Task<IActionResult> ExecuteCommand(ICommand<T> command)
        {
            var commandResult = await _mediator.Send(command);

            return commandResult switch
            {
                Success success => success.Value != null
                    ? new OkObjectResult(success.Value)
                    : new OkResult(),
                InvalidRequest failure => new BadRequestObjectResult(new {failure.Errors}),
                Unauthorized _ => new UnauthorizedResult(),
                ValidationResult vr => new BadRequestObjectResult(vr),
                _ => new StatusCodeResult(500)
            };
        }
    }
}