﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Activities.Queries;
using Application.Commands;
using Domain.Dtos;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    public class ActivitiesController : AbstractV1Controller<ActivityDto>
    {
        public ActivitiesController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<ActivityDto>>> GetActivities(
            CancellationToken cancellationToken)
        {
            var result = await Mediator.Send(new ActivitiesQuery(), cancellationToken);
            return HandleQueryListResult(result);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateActivity([FromBody] ActivityDto activityDto)
        {
            return await ExecuteCommand(new UpdateCommand<ActivityDto> {Dto = activityDto});
        }

        [HttpPost]
        public async Task<IActionResult> CreateActivity([FromBody] ActivityDto activityDto)
        {
            return await ExecuteCommand(new CreateCommand<ActivityDto> {Dto = activityDto});
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteActivity(Guid id)
        {
            return await ExecuteCommand(new DeleteCommand<ActivityDto> {Dto = new ActivityDto {Id = id}});
        }
    }
}