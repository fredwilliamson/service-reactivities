﻿using System.Threading.Tasks;
using Application.Commands;
using Domain.Dtos;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    public class AttendanceController: AbstractV1Controller<ActivityAttendeeDto>
    {
        public AttendanceController(IMediator mediator) : base(mediator)
        {
        }
        
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateActivity([FromBody] ActivityAttendeeDto activityAttendeeDto)
        {
            return await ExecuteCommand(new UpdateCommand<ActivityAttendeeDto> {Dto = activityAttendeeDto});
        }
    }
}