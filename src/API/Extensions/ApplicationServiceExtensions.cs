﻿using System;
using System.Text;
using Application.Activities.Commands.Handlers.Validation;
using Application.Activities.Commands.Handlers.Validation.Constants;
using Application.Activities.Queries;
using Application.Commands;
using Application.Commands.Handler.Validation;
using Application.Core;
using Application.Queries;
using Domain.Dtos;
using Infrastructure.Security;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Persistence;
using Persistence.Repositories;

namespace API.Extensions
{
    public static class ApplicationServiceExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services, IConfiguration config)
        {
            services.AddAutoMapper(typeof(MappingsProfile));
            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo {Title = "API", Version = "v1"}); });
            services.AddDbContext<DataContext>(opts => { opts.UseNpgsql(config.GetConnectionString("batchDb")); },
                ServiceLifetime.Transient);

            services.AddCors(opts =>
            {
                opts.AddPolicy("CorsPolicy", policy =>
                {
                    policy.WithMethods("GET", "PUT", "POST", "OPTIONS", "HEAD", "DELETE")
                        .WithOrigins("http://localhost:3000").WithHeaders("X-Requested-With", "Origin", "Content-Type",
                            "Accept", "Authorization");
                });
            });

            services.AddMediator(o =>
            {
                o.AddPipelineForEFCoreTransaction<DataContext>(options =>
                {
                    options.BeginTransactionOnCommand = true;
                });
                o.AddHandlersFromAssemblyOf<Startup>();
            });

            services.AddMediatR(typeof(ActivitiesQuery)).AddMediatR(typeof(KeyQuery<>))
                .AddMediatR(typeof(CreateCommand<>)).AddMediatR(typeof(UpdateCommand<>))
                .AddMediatR(typeof(DeleteCommand<>));

            services.AddTransient(typeof(IActivityRepository), typeof(ActivityRepository));
            services.AddTransient(typeof(IAttendeeRepository), typeof(AttendeeRepository));
            services.AddTransient(typeof(IUserRepository), typeof(UserRepository));
            
            services.AddScoped<IValidationRule<ActivityDto>, ExistingActivityTitleValidation>();
            services.AddScoped<IValidationRule<ActivityDto>, UnknownActivityEntity>();

            services.AddScoped<ExistingActivityTitleValidation>();
            services.AddScoped<UnknownActivityEntity>();
            
            services.AddTransient<Func<ActivityValidationTypeEnum, IValidationRule<ActivityDto>>>(serviceProvider =>
                key =>
                {
                    switch (key)
                    {
                        case ActivityValidationTypeEnum.ActivityValidationTitle:
                            return serviceProvider.GetService<ExistingActivityTitleValidation>();
                        case ActivityValidationTypeEnum.ActivityValidationUnknown:
                            return serviceProvider.GetService<UnknownActivityEntity>();
                        default:
                            return null;
                    }
                });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IUserAccessor,UserAccessor>();
            
            //Adding Athentication - JWT
            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(o =>
                {
                    o.RequireHttpsMetadata = false;
                    o.SaveToken = false;
                    o.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["JWT:Key"]))
                    };
                });

            return services;
        }
    }
}