﻿using AutoMapper;
using Microsoft.Extensions.Logging;

namespace Application
{
    public abstract class AbstractHandler
    {
        public AbstractHandler(IMapper mapper,ILogger logger)
        {
            Mapper = mapper;
            Logger = logger;
        }

        protected IMapper Mapper { get; }

        protected ILogger Logger { get; }
    }
}