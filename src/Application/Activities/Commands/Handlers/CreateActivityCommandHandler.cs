﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Activities.Commands.Handlers.Validation;
using Application.Activities.Commands.Handlers.Validation.Constants;
using Application.Commands;
using Application.Commands.Handler;
using Application.Commands.Handler.Validation;
using AutoMapper;
using Domain.Dtos;
using Domain.Models;
using Infrastructure.Security;
using MediatR;
using Microsoft.Extensions.Logging;
using Persistence.Repositories;

namespace Application.Activities.Commands.Handlers
{
    public class CreateActivityCommandHandler : AbstractCudHandler,
        IRequestHandler<CreateCommand<ActivityDto>, CommandResult>
    {
        private readonly IActivityRepository _repository;
        private readonly IUserAccessor _userAccessor;
        private readonly IUserRepository _userRepository;
        private readonly IValidationRule<ActivityDto> _validationRule;

        public CreateActivityCommandHandler(IMapper mapper,
            ILogger<CreateActivityCommandHandler> logger, IActivityRepository repository,
            Func<ActivityValidationTypeEnum, IValidationRule<ActivityDto>> serviceResolver,IUserAccessor userAccessor,IUserRepository userRepository) : base(mapper, logger)
        {
            _repository = repository;
            _userAccessor = userAccessor;
            _userRepository = userRepository;
            _validationRule = serviceResolver(ActivityValidationTypeEnum.ActivityValidationTitle);
        }

        /// <summary>
        ///     Creation of activity with previous validations
        /// </summary>
        /// <param name="request"> command with activity dto</param>
        /// <param name="cancellationToken">cancellation if request too long</param>
        /// <returns>success if create is ok else validation result</returns>
        public async Task<CommandResult> Handle(CreateCommand<ActivityDto> request, CancellationToken cancellationToken)
        {
            var entityToCreate = Mapper.Map(request.Dto, new Activity());
            var listMessages = await _validationRule.Validate(request.Dto);
            if (listMessages.Any()) return CommandResult.ValidationResult(listMessages);
            var user = await FindCurrentUser();
            entityToCreate.Attendees.Add(
                new ActivityAttendee
                {
                    Activity = entityToCreate,
                    UserId = user.Id,
                    IsHost = true
                }
        );
            var entityCreated = Mapper.Map(await _repository.CreateEntity(entityToCreate), new ActivityDto());
            return CommandResult.Success(entityCreated);
        }

        private Task<User> FindCurrentUser()
        {
            var userName = _userAccessor.GetUserName();
            return _userRepository.FindByUserName(userName);
        }
    }
}