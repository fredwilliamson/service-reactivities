﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Activities.Commands.Handlers.Validation;
using Application.Activities.Commands.Handlers.Validation.Constants;
using Application.Commands;
using Application.Commands.Handler.Validation;
using AutoMapper;
using Domain.Dtos;
using MediatR;
using Microsoft.Extensions.Logging;
using Persistence.Repositories;

namespace Application.Activities.Commands.Handlers
{
    public class DeleteActivityCommandHandler : AbstractHandler,
        IRequestHandler<DeleteCommand<ActivityDto>, CommandResult>
    {
        private readonly IActivityRepository _repository;
        private readonly IValidationRule<ActivityDto> _validationRule;
        public DeleteActivityCommandHandler(IMapper mapper,
            ILogger<DeleteActivityCommandHandler> logger, IActivityRepository repository,Func<ActivityValidationTypeEnum, IValidationRule<ActivityDto>> serviceResolver) : base(mapper, logger)
        {
            _repository = repository;
            _validationRule = serviceResolver(ActivityValidationTypeEnum.ActivityValidationUnknown);
        }
        /// <summary>
        ///  Delete activity with previous validations
        /// </summary>
        /// <param name="request"> command with activity dto</param>
        /// <param name="cancellationToken">cancellation if request too long</param>
        /// <returns>success if delete is ok else validation result</returns>
        public async Task<CommandResult> Handle(DeleteCommand<ActivityDto> request, CancellationToken cancellationToken)
        {
            var id = request.Dto.Id;
            var listMessages= await _validationRule.Validate(request.Dto);
            if (listMessages.Any())
            {
                return CommandResult.ValidationResult(listMessages);
            }
            var activity = await _repository.FindEntityById(id);
            await _repository.DeleteEntity(activity);

            return CommandResult.Success();
        }
    }
}