﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Activities.Commands.Handlers.Validation;
using Application.Activities.Commands.Handlers.Validation.Constants;
using Application.Commands;
using Application.Commands.Handler;
using Application.Commands.Handler.Validation;
using AutoMapper;
using Domain.Dtos;
using MediatR;
using Microsoft.Extensions.Logging;
using Persistence.Repositories;

namespace Application.Activities.Commands.Handlers
{
    public class UpdateActivityCommandHandler : AbstractCudHandler,
        IRequestHandler<UpdateCommand<ActivityDto>, CommandResult>
    {
        private readonly IActivityRepository _repository;
        private readonly IEnumerable<IValidationRule<ActivityDto>> _validationRules;

        public UpdateActivityCommandHandler(IMapper mapper,
            ILogger<UpdateActivityCommandHandler> logger, IActivityRepository repository,
            Func<ActivityValidationTypeEnum, IValidationRule<ActivityDto>> serviceResolver) : base(mapper, logger)
        {
            _repository = repository;
            _validationRules = new List<IValidationRule<ActivityDto>>
            {
                serviceResolver(ActivityValidationTypeEnum.ActivityValidationUnknown),
                serviceResolver(ActivityValidationTypeEnum.ActivityValidationTitle)
            };
        }

        /// <summary>
        ///     Update activity with previous validations
        /// </summary>
        /// <param name="request"> command with activity dto</param>
        /// <param name="cancellationToken">cancellation if request too long</param>
        /// <returns>success if update is ok else validation result</returns>
        public async Task<CommandResult> Handle(UpdateCommand<ActivityDto> request, CancellationToken cancellationToken)
        {
            ActivityDto param = request.Dto;
            var messagesGrouped = await UpdateValidation(param);
            // flat messages
            var messages = messagesGrouped.SelectMany(i => i);
            if (messages.Any()) return CommandResult.ValidationResult(messages);

            var activity = await _repository.FindEntityById(param.Id);

            activity = Mapper.Map(param, activity);

            var entityCreated = Mapper.Map(await _repository.UpdateEntity(activity), new ActivityDto());
            return CommandResult.Success(entityCreated);
        }

        private async Task<IEnumerable<ValidationMessage>[]> UpdateValidation(ActivityDto activity)
        {
            var listMessages = new List<Task<IEnumerable<ValidationMessage>>>();
            foreach (var validationRule in _validationRules) listMessages.Add((Task<IEnumerable<ValidationMessage>>) validationRule.Validate(activity));

            return await Task.WhenAll(listMessages);
        }
    }
}