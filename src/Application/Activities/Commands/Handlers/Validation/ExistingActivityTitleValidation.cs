﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Activities.Commands;
using Application.Commands;
using Application.Commands.Handler.Validation;
using Domain.Dtos;
using Domain.Models;
using Persistence.Repositories;

namespace Application.Activities.Commands.Handlers.Validation
{
    public class ExistingActivityTitleValidation : IValidationRule<ActivityDto>
    {
        private readonly IActivityRepository _repository;

        public ExistingActivityTitleValidation(IActivityRepository repository)
        {
            _repository = repository;
        }
        /// <summary>
        ///  Check if activity with same title already exists in db
        /// </summary>
        /// <param name="obj">activity dto</param>
        /// <returns>validation message if same title found in db</returns>
        public async Task<IEnumerable<ValidationMessage>> Validate(ActivityDto obj)
        {
            var validationMessages = new List<ValidationMessage>();
            IEnumerable<Activity> activities = await _repository.FindByTitle(obj.Title);
            
            if (activities.Any(activity => !activity.Id.Equals(obj.Id)))
            {
                validationMessages.Add(ValidationMessage.ToError("Title already exists in db"));
            }

            return validationMessages;
        }
    }
}