﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Activities.Commands;
using Application.Commands;
using Application.Commands.Handler.Validation;
using Domain.Dtos;
using Persistence.Repositories;

namespace Application.Activities.Commands.Handlers.Validation
{
    public class UnknownActivityEntity : IValidationRule<ActivityDto>
    {
        private readonly IActivityRepository _repository;

        public UnknownActivityEntity(IActivityRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Check if activity id exists in db
        /// </summary>
        /// <param name="obj">activity dto</param>
        /// <returns>validation message if id not in db</returns>
        public async Task<IEnumerable<ValidationMessage>> Validate(ActivityDto obj)
        {
            var validationMessages = new List<ValidationMessage>();
            Guid id = obj.Id;
            var activity = await _repository.FindEntityById(id);
            
            if (activity == null)
                validationMessages.Add(ValidationMessage.ToError($"activity with id {id} not found"));
            
            return validationMessages;
        }
    }
}