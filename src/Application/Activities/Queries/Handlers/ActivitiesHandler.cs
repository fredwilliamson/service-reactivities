﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Commands;
using AutoMapper;
using Domain.Dtos;
using MediatR;
using Microsoft.Extensions.Logging;
using Persistence.Repositories;

namespace Application.Activities.Queries.Handlers
{
    public class ActivitiesHandler : AbstractHandler,
        IRequestHandler<ActivitiesQuery, IResultQuery<ICollection<ActivityDto>>>
    {
        private readonly IActivityRepository _repository;

        public ActivitiesHandler(ILogger<ActivitiesHandler> logger, IMapper mapper, IActivityRepository repository) :
            base(mapper, logger)
        {
            _repository = repository;
        }

        public async Task<IResultQuery<ICollection<ActivityDto>>> Handle(ActivitiesQuery request,
            CancellationToken cancellationToken)
        {
            return ResultQuery<ICollection<ActivityDto>>.Success(
                await _repository.FindAllActivities(cancellationToken));
        }
    }
}