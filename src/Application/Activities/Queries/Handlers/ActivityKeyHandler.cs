﻿using System.Threading;
using System.Threading.Tasks;
using Application.Commands;
using Application.Queries;
using AutoMapper;
using Domain.Dtos;
using MediatR;
using Microsoft.Extensions.Logging;
using Persistence.Repositories;

namespace Application.Activities.Queries.Handlers
{
    public class ActivityKeyHandler : AbstractHandler, IRequestHandler<KeyQuery<ActivityDto>, IResultQuery<ActivityDto>>
    {
        private readonly IActivityRepository _repository;

        public ActivityKeyHandler(ILogger<ActivitiesHandler> logger, IMapper mapper, IActivityRepository repository) :
            base(mapper, logger)
        {
            _repository = repository;
        }

        public virtual async Task<IResultQuery<ActivityDto>> Handle(KeyQuery<ActivityDto> request,
            CancellationToken cancellationToken)
        {
            var activity = await _repository.FindEntityById(request.Id);
            return ResultQuery<ActivityDto>.Success(Mapper.Map(activity, new ActivityDto()));
        }
    }
}