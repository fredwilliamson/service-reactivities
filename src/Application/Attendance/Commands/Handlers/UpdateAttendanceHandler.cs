﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Commands;
using Application.Commands.Handler;
using AutoMapper;
using Domain.Dtos;
using Domain.Models;
using Infrastructure.Security;
using MediatR;
using Microsoft.Extensions.Logging;
using Persistence.Repositories;

namespace Application.Attendance.Commands.Handlers
{
    public class UpdateAttendanceHandler: AbstractCudHandler,IRequestHandler<UpdateCommand<ActivityAttendeeDto>, CommandResult>
    {
        private readonly IAttendeeRepository _attendeeRepository;
        private readonly IUserAccessor _userAccessor;
        private readonly IActivityRepository _activityRepository;
        private readonly IUserRepository _userRepository;

        public UpdateAttendanceHandler(IMapper mapper, ILogger<UpdateAttendanceHandler> logger,IAttendeeRepository attendeeRepository,
            IUserAccessor userAccessor,IActivityRepository activityRepository,IUserRepository userRepository) : base(mapper, logger)
        {
            _attendeeRepository = attendeeRepository;
            _userAccessor = userAccessor;
            _activityRepository = activityRepository;
            _userRepository = userRepository;
        }

        public async Task<CommandResult> Handle(UpdateCommand<ActivityAttendeeDto> request, CancellationToken cancellationToken)
        {
           var activity = await _activityRepository.FindActivityWithAttendees(request.Dto.ActivityId);
           var user = await _userRepository.FindByUserName(_userAccessor.GetUserName());
           var hostName = activity.Attendees.FirstOrDefault(aa => aa.IsHost)?.User?.UserName;
           var attendance = activity.Attendees.SingleOrDefault(aa => aa.User.UserName == user.UserName);
           
           if (attendance!=null && user.UserName.Equals(hostName))
           {
               activity.IsCancelled = !activity.IsCancelled;
           }
           
           if (attendance!=null && !user.UserName.Equals(hostName))
           {
               activity.Attendees.Remove(attendance);
           }

           if (attendance==null)
           {
               attendance = new ActivityAttendee {ActivityId = activity.Id,UserId = user.Id,IsHost = hostName==null};
               await _attendeeRepository.CreateEntity(attendance);
           }
           await _attendeeRepository.SaveChangeAsync();
           
           return CommandResult.Success();
        }
    }
}