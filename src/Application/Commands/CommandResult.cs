using System.Collections.Generic;

namespace Application.Commands
{
    public abstract class CommandResult
    {
        public static CommandResult InvalidRequest(params string[] errors)
        {
            return new InvalidRequest(errors);
        }

        public static CommandResult Success(object val)
        {
            return new Success(val);
        }

        public static CommandResult Success()
        {
            return new Success();
        }

        public static CommandResult ServerError()
        {
            return new ServerError();
        }

        public static CommandResult NotFoundError(IEnumerable<string> errors)
        {
            return new NotFoundError(errors);
        }

        public static CommandResult Unauthorized()
        {
            return new Unauthorized();
        }

        public static ValidationResult ValidationResult(IEnumerable<ValidationMessage> messages)
        {
            return new(messages);
        }
    }

    public class Success : CommandResult
    {
        public Success()
        {
        }

        public Success(object value)
        {
            Value = value;
        }

        public object Value { get; }
    }

    public class InvalidRequest : CommandResult
    {
        public InvalidRequest(IEnumerable<string> errors)
        {
            Errors = errors;
        }

        public IEnumerable<string> Errors { get; }
    }

    public class NotFoundError : CommandResult
    {
        public NotFoundError(IEnumerable<string> errors)
        {
            Errors = errors;
        }

        public IEnumerable<string> Errors { get; }
    }


    public class ServerError : CommandResult
    {
    }

    public class Unauthorized : CommandResult
    {
    }
}