﻿using Domain;

namespace Application.Commands
{
    public class CreateCommand<T> : ICommand<T> where T : IPrimaryKey
    {
        public T Dto { get; set; }
    }
}