﻿using Domain;

namespace Application.Commands
{
    public class DeleteCommand<T> : ICommand<T> where T : IPrimaryKey
    {
        public T Dto { get; set; }
    }
}