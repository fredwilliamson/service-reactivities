﻿using AutoMapper;
using Microsoft.Extensions.Logging;

namespace Application.Commands.Handler
{
    public class AbstractCudHandler : AbstractHandler
    {
        public AbstractCudHandler(IMapper mapper, ILogger logger) : base(mapper, logger)
        {
        }
    }
}