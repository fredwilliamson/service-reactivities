﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Commands.Handler.Validation
{
    public interface IValidationRule<E>
    {
        Task<IEnumerable<ValidationMessage>> Validate(E obj);
    }
}