﻿using MediatR;

namespace Application.Commands
{
    public interface ICommand<T> : IRequest<CommandResult>
    {
        T Dto { get; set; }
    }
}