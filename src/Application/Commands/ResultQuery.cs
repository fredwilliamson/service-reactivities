﻿namespace Application.Commands
{
    public class ResultQuery<T> : IResultQuery<T>
    {
        public bool IsSuccess { get; set; }

        public T Value { get; set; }

        public string Message { get; set; }

        public static ResultQuery<T> Success(T value)
        {
            return new() {IsSuccess = true, Value = value};
        }

        public static ResultQuery<T> Error(string message)
        {
            return new() {IsSuccess = false, Message = message};
        }
    }
}