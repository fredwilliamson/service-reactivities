﻿using Domain;

namespace Application.Commands
{
    public class UpdateCommand<T> : ICommand<T>
    {
        public T Dto { get; set; }
    }
}