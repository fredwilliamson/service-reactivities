﻿using System.Collections.Generic;

namespace Application.Commands
{
    public class ValidationResult : CommandResult
    {
        public ValidationResult(IEnumerable<ValidationMessage> messages)
        {
            Messages = messages;
        }

        public IEnumerable<ValidationMessage> Messages { get; }
    }

    public class ValidationMessage
    {
        public bool IsWarning { get; set; }

        public string Message { get; set; }

        public static ValidationMessage ToError(string error)
        {
            return new() {Message = error};
        }

        public static ValidationMessage ToWarning(string warning)
        {
            return new() {Message = warning, IsWarning = true};
        }
    }
}