﻿using AutoMapper;
using Domain.Dtos;
using Domain.Models;

namespace Application.Core
{
    public class MappingsProfile : Profile
    {
        public MappingsProfile()
        {
            CreateMap<Activity, ActivityDto>();
            CreateMap<ActivityDto, Activity>();
            CreateMap<User, UserDto>();
            CreateMap<ActivityAttendee, ActivityAttendeeDto>();
        }
    }
}