﻿using System;
using Application.Commands;
using Domain;
using MediatR;

namespace Application.Queries
{
    public interface IKeyQuery<T> : IRequest<IResultQuery<T>> where T : IPrimaryKey
    {
        Guid Id { get; set; }
    }
}