﻿using System.Collections.Generic;
using Application.Commands;
using Domain;
using MediatR;

namespace Application.Queries
{
    public interface IQueryList<T> : IRequest<IResultQuery<ICollection<T>>> where T : IPrimaryKey
    {
    }
}