﻿using System;
using Domain;

namespace Application.Queries
{
    public class KeyQuery<T> : IKeyQuery<T> where T : IPrimaryKey
    {
        public Guid Id { get; set; }
    }
}