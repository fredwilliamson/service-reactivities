﻿using System;

namespace Domain.Dtos
{
    public class ActivityAttendeeDto : IPrimaryKey
    {
        public Guid ActivityId { get; set; }
        public UserDto User { get; set; }
        public Guid Id => ActivityId;
    }
}