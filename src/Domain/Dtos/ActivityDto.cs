﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Dtos
{
    public class ActivityDto : IPrimaryKey
    {
        [Required]
        [MinLength(2)]
        [MaxLength(250)]
        public string Title { get; set; }


        [Required] public DateTime Date { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(250)]
        public string Description { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string Category { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string City { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string Venue { get; set; }

        public Guid Id { get; set; }
        
        public string HostedBy { get; set; }
        
        public bool IsCancelled { get; set; }
        public IEnumerable<UserProfile> Attendees { get; set; } = new List<UserProfile>();
    }
}