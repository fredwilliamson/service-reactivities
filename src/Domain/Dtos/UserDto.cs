﻿namespace Domain.Dtos
{
    public class UserDto
    {
        public string DisplayName { get; set; }
    }
}