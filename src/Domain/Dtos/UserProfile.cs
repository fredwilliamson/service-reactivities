﻿namespace Domain.Dtos
{
    public class UserProfile
    {
        public string DisplayName { get; set; }
        public string Bio { get; set; }
    }
}