﻿using System;

namespace Domain
{
    public interface IPrimaryKey
    {
        Guid Id { get; }
    }
}