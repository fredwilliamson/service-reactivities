﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Domain.Models
{
    public class User : IdentityUser
    {
        [MinLength(2)]
        [MaxLength(100)]
        public string DisplayName { get; set; }
        
        [MinLength(2)]
        [MaxLength(250)]
        public string Bio { get; set; }
        public ICollection<ActivityAttendee> Activities { get; set; }
    }
}