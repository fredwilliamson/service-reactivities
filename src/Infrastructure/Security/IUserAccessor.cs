﻿namespace Infrastructure.Security
{
    public interface IUserAccessor
    {
        string GetUserName();
    }
}