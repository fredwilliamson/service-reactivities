﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Persistence.Repositories;

namespace Infrastructure.Security
{
    public class IsHostRequirement: IAuthorizationRequirement
    {
    }
    public class IsHostRequirementHandler: AuthorizationHandler<IsHostRequirement>
    {
        private readonly IActivityRepository _activityRepository;
        private readonly HttpContextAccessor _httpContextAccessor;
        private readonly IUserRepository _userRepository;

        public IsHostRequirementHandler(IActivityRepository activityRepository,HttpContextAccessor httpContextAccessor, IUserRepository userRepository)
        {
            _activityRepository = activityRepository;
            _httpContextAccessor = httpContextAccessor;
            _userRepository = userRepository;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, IsHostRequirement requirement)
        {
            return null;
        }
    }
    
}