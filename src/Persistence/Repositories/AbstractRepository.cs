﻿using System;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public abstract class AbstractRepository<E> : IRepository<E> where E : class

    {
        public AbstractRepository(DataContext context)
        {
            Context = context;
            EntityName = FindEntityName();
        }

        protected DataContext Context { get; }

        protected string EntityName { get; }

        public virtual async Task<E> FindEntityById(Guid id)
        {
            return await Context.FindAsync<E>(id);
        }

        private static string FindEntityName()
        {
            return typeof(E).Name;
        }

        public virtual async Task<E> CreateEntity(E entity)
        {
            var entityCreated = await Context.AddAsync(entity);
            await Context.SaveChangesAsync();

            return (E) entityCreated.Entity;
        }

        public virtual async Task<E> UpdateEntity(E entity)
        {
            var entityUpdated = Context.Update(entity).Entity;
            await Context.SaveChangesAsync();

            return (E) entityUpdated;
        }

        public virtual  async Task<E> DeleteEntity(E entity)
        {
            var entityDeleted = Context.Remove(entity).Entity;
            await Context.SaveChangesAsync();
            return (E) entityDeleted;
        }

        public async Task<int> SaveChangeAsync()
        {
            return await Context.SaveChangesAsync();
        }
    }
}