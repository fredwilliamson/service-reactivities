﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Domain.Dtos;
using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Persistence.Repositories
{
    public class ActivityRepository : AbstractRepository<Activity>, IActivityRepository
    {
        public ActivityRepository(DataContext context) : base(context)
        {
        }
        
        public async Task<ICollection<ActivityDto>> FindAllActivities(CancellationToken cancellationToken)
        {
            var resultList= await  Context.Activities
                .Include(a => a.Attendees)
                .ThenInclude(aa => aa.User)
                
                .Select(a =>
                    new 
                    {
                        a.Id,
                        a.Category,
                        a.City,
                        a.Date,
                        a.Description,
                        a.Title,
                        a.Venue,
                        a.IsCancelled,
                        HostedBy = a.Attendees.Where(aa => aa.IsHost).Select(aa=>aa.User.DisplayName),
                        Attendees = a.Attendees.Select(aa => aa.User).Select(u => new 
                            {u.DisplayName, u.Bio})
                    }
                ).ToListAsync(cancellationToken);
            return   resultList.Select(res => new ActivityDto
            {
                Id = res.Id,
                Category = res.Category,
                City = res.City,
                Date = res.Date,
                Description = res.Description,
                Title = res.Title,
                Venue = res.Venue,
                IsCancelled = res.IsCancelled,
                HostedBy = res.HostedBy.FirstOrDefault(),
                Attendees = res.Attendees.Select(aa=>new UserProfile
                {
                    Bio = aa.Bio,
                    DisplayName = aa.DisplayName
                })
            }).ToList();
        }

        public async Task<IEnumerable<Activity>> FindByTitle(string title)
        {
            return  await Context.Activities.Where(activity => activity.Title == title).ToListAsync();
        }

        public async Task<Activity> FindActivityWithAttendees(Guid activityId)
        {
            return await Context.Activities.Include(a => a.Attendees).ThenInclude(aa => aa.User)
                .SingleOrDefaultAsync(a => a.Id.Equals(activityId));
        }
    }
}