﻿using System;
using System.Threading.Tasks;
using Domain.Dtos;
using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Persistence.Repositories
{
    public class AttendeeRepository: AbstractRepository<ActivityAttendee>, IAttendeeRepository
    {
        public AttendeeRepository(DataContext context) : base(context)
        {
        }

        public async Task<ActivityAttendee> FindAttendanceByUserNameAndActivityId(string userName,Guid activityId)
        {
            return await Context.ActivityAttendees.SingleOrDefaultAsync(aa =>
                aa.User.UserName.ToLower().Equals(userName.ToLower()) && aa.ActivityId==activityId);
        }
    }
}