﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Domain.Dtos;
using Domain.Models;

namespace Persistence.Repositories
{
    public interface IActivityRepository: IRepository<Activity>
    {
        Task<ICollection<ActivityDto>> FindAllActivities(CancellationToken cancellationToken);

        Task<IEnumerable<Activity>> FindByTitle(string title);

        Task<Activity> FindActivityWithAttendees(Guid activityId);
    }
}