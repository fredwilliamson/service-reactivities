﻿using System;
using System.Threading.Tasks;
using Domain.Models;

namespace Persistence.Repositories
{
    public interface IAttendeeRepository: IRepository<ActivityAttendee>
    {
        Task<ActivityAttendee> FindAttendanceByUserNameAndActivityId(string userName,Guid activityId);
    }
}