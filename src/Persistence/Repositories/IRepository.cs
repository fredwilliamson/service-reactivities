﻿using System;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public interface IRepository<E> where E : class 
    {
        Task<E> FindEntityById(Guid id);
        Task<E> CreateEntity(E entity);
        Task<E> UpdateEntity(E entity);
        Task<E> DeleteEntity(E entity);

        Task<int> SaveChangeAsync();
    }
}