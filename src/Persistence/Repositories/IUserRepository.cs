﻿using System.Threading.Tasks;
using Domain.Models;

namespace Persistence.Repositories
{
    public interface IUserRepository: IRepository<User>
    {
        Task<User> FindByUserName(string name);
    }
}