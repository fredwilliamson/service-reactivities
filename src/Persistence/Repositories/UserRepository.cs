﻿using System.Linq;
using System.Threading.Tasks;
using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Persistence.Repositories
{
    public class UserRepository: AbstractRepository<User>,IUserRepository
    {
        public UserRepository(DataContext context) : base(context)
        {
        }

        public async Task<User> FindByUserName(string name)
        {
            return await Context.Users.Where(u => u.UserName.ToLower().Equals(name.ToLower())).FirstAsync();
        }
    }
}