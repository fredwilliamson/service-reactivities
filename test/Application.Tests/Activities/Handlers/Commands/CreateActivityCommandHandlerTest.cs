﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Activities.Commands.Handlers;
using Application.Activities.Commands.Handlers.Validation.Constants;
using Application.Commands;
using Application.Commands.Handler.Validation;
using AutoMapper;
using Domain.Dtos;
using Domain.Models;
using Infrastructure.Security;
using Microsoft.Extensions.Logging;
using Moq;
using Persistence.Repositories;
using Xunit;

namespace Application.Tests.Activities.Handlers.Commands
{
    public class CreateActivityCommandHandlerTest
    {
        private CreateActivityCommandHandler _handler;
        
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<CreateActivityCommandHandler>> _loggerMock;
        private Mock<IActivityRepository> _repository;
        private Mock<Func<ActivityValidationTypeEnum, IValidationRule<ActivityDto>>>_functionMock;
        private Mock<IValidationRule<ActivityDto>>_validationMock;
        private Mock<IUserAccessor> _userAccessorMock;
        private Mock<IUserRepository> _userRepository;

        public CreateActivityCommandHandlerTest()
        {
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<CreateActivityCommandHandler>>();
            _repository = new Mock<IActivityRepository>();
            _functionMock = new Mock<Func<ActivityValidationTypeEnum, IValidationRule<ActivityDto>>>();
            _validationMock = new Mock<IValidationRule<ActivityDto>>();
            _userAccessorMock = new Mock<IUserAccessor>();
            _userRepository = new Mock<IUserRepository>();

            _functionMock.Setup(fun => fun.Invoke(ActivityValidationTypeEnum.ActivityValidationTitle))
                .Returns(_validationMock.Object);
            
            _handler = new CreateActivityCommandHandler(_mapperMock.Object, _loggerMock.Object, _repository.Object, _functionMock.Object,_userAccessorMock.Object,_userRepository.Object);
        }

        [Fact]
        public async void WhenValidationReturnMessagesThenNoCreation()
        {
            // WHEN
            ActivityDto activityDto = ObjectTestUtils.BuildActivityDto();
            var messages = ObjectTestUtils.BuilValidationMessages();
            _validationMock.Setup(valid => valid.Validate(activityDto))
                .Returns(Task.FromResult<IEnumerable<ValidationMessage>>(messages));
            
            // EXECUTE
            CommandResult result = await _handler.Handle(new CreateCommand<ActivityDto> {Dto = activityDto},
                new CancellationToken());

            // ASSERT
            _repository.Verify(repo=>repo.CreateEntity(It.IsAny<Activity>()),Times.Never());
        }
        
        [Fact]
        public async void WhenValidationReturnMessagesThenReturnValidationResultWithMessages()
        {
            // WHEN
            ActivityDto activityDto = ObjectTestUtils.BuildActivityDto();
            var messages = ObjectTestUtils.BuilValidationMessages();
            _validationMock.Setup(valid => valid.Validate(activityDto))
                .Returns(Task.FromResult<IEnumerable<ValidationMessage>>(messages));
            
            // EXECUTE
            CommandResult result = await _handler.Handle(new CreateCommand<ActivityDto> {Dto = activityDto},
                new CancellationToken());

            // ASSERT
            Assert.IsType<ValidationResult>(result);
            ValidationResult validationResult = (ValidationResult) result;
            Assert.StrictEqual(messages,validationResult.Messages);
        }
        
        [Fact]
        public async void WhenValidationReturnNotingThenCreation()
        {
            // WHEN
            var activityDto = ObjectTestUtils.BuildActivityDto();
            var messages = new List<ValidationMessage>();
            var id = Guid.NewGuid();
            _mapperMock.Setup(mapper => mapper.Map(activityDto, It.IsAny<Activity>()))
                .Returns(ObjectTestUtils.BuildActivity(id));
            _validationMock.Setup(valid => valid.Validate(activityDto))
                .Returns(Task.FromResult<IEnumerable<ValidationMessage>>(messages));
            _userAccessorMock.Setup(userAccessor => userAccessor.GetUserName()).Returns("Bob");
            _userRepository.Setup(userRepo => userRepo.FindByUserName("Bob")).ReturnsAsync(ObjectTestUtils.BuildUser);
            
            // EXECUTE
            CommandResult result = await _handler.Handle(new CreateCommand<ActivityDto> {Dto = activityDto},
                new CancellationToken());

            // ASSERT
            _repository.Verify(repo=>repo.CreateEntity(It.IsAny<Activity>()),Times.Once());
            Assert.IsType<Success>(result);
        }

    }
}