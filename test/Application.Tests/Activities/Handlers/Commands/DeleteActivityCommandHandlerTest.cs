﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Activities.Commands.Handlers;
using Application.Commands.Handler.Validation;
using Application.Activities.Commands.Handlers.Validation.Constants;
using Application.Commands;
using AutoMapper;
using Domain.Dtos;
using Domain.Models;
using Microsoft.Extensions.Logging;
using Moq;
using Persistence.Repositories;
using Xunit;

namespace Application.Tests.Activities.Handlers.Commands
{
    public class DeleteActivityCommandHandlerTest
    {
        private readonly DeleteActivityCommandHandler _handler;
        private readonly Mock<IActivityRepository> _repository;
        private readonly Mock<IValidationRule<ActivityDto>>_validationMock;

        public DeleteActivityCommandHandlerTest()
        {
            var mapperMock = new Mock<IMapper>();
            var loggerMock = new Mock<ILogger<DeleteActivityCommandHandler>>();
            _repository = new Mock<IActivityRepository>();
            var functionMock = new Mock<Func<ActivityValidationTypeEnum, IValidationRule<ActivityDto>>>();
            _validationMock = new Mock<IValidationRule<ActivityDto>>();

            functionMock.Setup(fun => fun.Invoke(ActivityValidationTypeEnum.ActivityValidationUnknown))
                .Returns(_validationMock.Object);
            
            _handler = new DeleteActivityCommandHandler(mapperMock.Object, loggerMock.Object, _repository.Object, functionMock.Object);
        }
        
        [Fact]
        public async void WhenValidationReturnMessagesThenNoDeletion()
        {
            // WHEN
            ActivityDto activityDto = ObjectTestUtils.BuildActivityDto();
            var messages = ObjectTestUtils.BuilValidationMessages();
            _validationMock.Setup(valid => valid.Validate(activityDto))
                .Returns(Task.FromResult<IEnumerable<ValidationMessage>>(messages));
            
            // EXECUTE
            CommandResult result = await _handler.Handle(new DeleteCommand<ActivityDto>{Dto = activityDto},
                new CancellationToken());

            // ASSERT
            _repository.Verify(repo=>repo.DeleteEntity(It.IsAny<Activity>()),Times.Never());
        }
        
        [Fact]
        public async void WhenValidationReturnMessagesThenReturnValidationResultWithMessages()
        {
            // WHEN
            ActivityDto activityDto = ObjectTestUtils.BuildActivityDto();
            var messages = ObjectTestUtils.BuilValidationMessages();
            _validationMock.Setup(valid => valid.Validate(activityDto))
                .Returns(Task.FromResult<IEnumerable<ValidationMessage>>(messages));
            
            // EXECUTE
            CommandResult result = await _handler.Handle(new DeleteCommand<ActivityDto>{Dto = activityDto},
                new CancellationToken());

            // ASSERT
            Assert.IsType<ValidationResult>(result);
            ValidationResult validationResult = (ValidationResult) result;
            Assert.StrictEqual(messages,validationResult.Messages);
        }
        
        [Fact]
        public async void WhenValidationReturnNotingThenDeletion()
        {
            // WHEN
            var activityDto = ObjectTestUtils.BuildActivityDto();
            activityDto.Id= Guid.NewGuid();
            var messages = new List<ValidationMessage>();
            _validationMock.Setup(valid => valid.Validate(activityDto))
                .Returns(Task.FromResult<IEnumerable<ValidationMessage>>(messages));
            var activity = ObjectTestUtils.BuildActivity(activityDto.Id);
            _repository.Setup(repo => repo.FindEntityById(activityDto.Id)).ReturnsAsync(activity);
            
            
            // EXECUTE
            CommandResult result = await _handler.Handle(new DeleteCommand<ActivityDto> {Dto = activityDto},
                new CancellationToken());

            // ASSERT
            _repository.Verify(repo=>repo.DeleteEntity(It.IsAny<Activity>()),Times.Once());
            Assert.IsType<Success>(result);
        }
    }
}