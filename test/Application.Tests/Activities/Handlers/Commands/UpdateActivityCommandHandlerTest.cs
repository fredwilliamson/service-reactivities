﻿using System;
using System.Collections.Generic;
using System.Threading;
using Application.Activities.Commands.Handlers;
using Application.Commands.Handler.Validation;
using Application.Activities.Commands.Handlers.Validation.Constants;
using Application.Commands;
using AutoMapper;
using Domain.Dtos;
using Domain.Models;
using Microsoft.Extensions.Logging;
using Moq;
using Persistence.Repositories;
using Xunit;

namespace Application.Tests.Activities.Handlers.Commands
{
    public class UpdateActivityCommandHandlerTest
    {
        private readonly UpdateActivityCommandHandler _handler;
        private readonly Mock<IActivityRepository> _repository;
        private readonly Mock<IValidationRule<ActivityDto>> _validationTitleMock;
        private readonly Mock<IValidationRule<ActivityDto>> _validationExistsMock;
        private readonly Mock<IMapper> _mapper;
        
        public UpdateActivityCommandHandlerTest()
        {
            _mapper = new Mock<IMapper>();
            var loggerMock = new Mock<ILogger<UpdateActivityCommandHandler>>();
            _repository = new Mock<IActivityRepository>();
            var functionMock = new Mock<Func<ActivityValidationTypeEnum, IValidationRule<ActivityDto>>>();
            _validationTitleMock = new Mock<IValidationRule<ActivityDto>>();
            _validationExistsMock= new Mock<IValidationRule<ActivityDto>>();
            
            functionMock.Setup(fun => fun.Invoke(ActivityValidationTypeEnum.ActivityValidationUnknown))
                .Returns(_validationExistsMock.Object);
            functionMock.Setup(fun => fun.Invoke(ActivityValidationTypeEnum.ActivityValidationTitle))
                .Returns(_validationTitleMock.Object);
            
            _handler = new UpdateActivityCommandHandler(_mapper.Object, loggerMock.Object, _repository.Object, functionMock.Object);
        }
        
        [Fact]
        public async void WhenValidationReturnMessagesThenNoDeletion()
        {
            // WHEN
            ActivityDto activityDto = ObjectTestUtils.BuildActivityDto();
            var messages = ObjectTestUtils.BuilValidationMessages();
            _validationTitleMock.Setup(valid => valid.Validate(activityDto))
                .ReturnsAsync(messages);
            
            // EXECUTE
            CommandResult result = await _handler.Handle(new UpdateCommand<ActivityDto> {Dto = activityDto},
                new CancellationToken());

            // ASSERT
            _repository.Verify(repo=>repo.UpdateEntity(It.IsAny<Activity>()),Times.Never());
        }
        
        [Fact]
        public async void WhenValidationReturnMessagesThenReturnValidationResultWithMessages()
        {
            // WHEN
            ActivityDto activityDto = ObjectTestUtils.BuildActivityDto();
            var messagesTitle = ObjectTestUtils.BuilValidationMessages();
            _validationTitleMock.Setup(valid => valid.Validate(activityDto))
                .ReturnsAsync(messagesTitle);
            var messagesUnknown = new List<ValidationMessage> { ValidationMessage.ToError("unknown activity")};
            _validationExistsMock.Setup(valid => valid.Validate(activityDto))
                .ReturnsAsync(messagesUnknown);
            
            // EXECUTE
            CommandResult result = await _handler.Handle(new UpdateCommand<ActivityDto> {Dto = activityDto},
                new CancellationToken());

            // ASSERT
            Assert.IsType<ValidationResult>(result);
            ValidationResult validationResult = (ValidationResult) result;
            Assert.Collection(validationResult.Messages,
                item => Assert.Equal("unknown activity", item.Message),
                item => Assert.Equal("Existing title", item.Message));
        }
        
        [Fact]
        public async void WhenValidationReturnNotingThenUpdate()
        {
            // WHEN
            var activityDto = ObjectTestUtils.BuildActivityDto();
            activityDto.City = "Vannes";
            activityDto.Id= Guid.NewGuid();
            var messages = new List<ValidationMessage>();
            _validationTitleMock.Setup(valid => valid.Validate(activityDto))
                .ReturnsAsync(messages);
            _validationExistsMock.Setup(valid => valid.Validate(activityDto))
                .ReturnsAsync(messages);
            var activity = ObjectTestUtils.BuildActivity(activityDto.Id);
            _repository.Setup(repo => repo.FindEntityById(activityDto.Id)).ReturnsAsync(activity);
            _mapper.Setup(map => map.Map(activityDto, activity)).Returns(new Activity()
            {
                Id = activityDto.Id, Category = activityDto.Category,
                City = activityDto.City, Title = activityDto.Title, Date = activityDto.Date,
                Description = activityDto.Description, Venue = activityDto.Venue
            });
            Activity activityExpected = null;
            _repository.Setup(repo => repo.UpdateEntity(It.IsAny<Activity>()))
                .Callback<Activity>(response => activityExpected = response);
            
            // EXECUTE
            CommandResult result = await _handler.Handle(new UpdateCommand<ActivityDto> {Dto = activityDto},
                new CancellationToken());

            // ASSERT
            _repository.Verify(repo=>repo.UpdateEntity(It.IsAny<Activity>()),Times.Once());
            Assert.IsType<Success>(result);
            Assert.Equal("Vannes",activityExpected.City);
        }
    }
}