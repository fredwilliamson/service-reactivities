﻿using System;
using System.Collections.Generic;
using Application.Activities.Commands.Handlers.Validation;
using Application.Commands;
using Domain.Models;
using Moq;
using Persistence.Repositories;
using Xunit;

namespace Application.Tests.Activities.Handlers.Commands.Validation
{
    public class ExistingActivityTitleValidationTest
    {
        private readonly ExistingActivityTitleValidation _validation;
        
        private readonly Mock<IActivityRepository> _repository;

        public ExistingActivityTitleValidationTest()
        {
            _repository = new Mock<IActivityRepository>();
            _validation = new ExistingActivityTitleValidation(_repository.Object);
        }

        [Fact]
        public async void WhenExistingTitleFoundThenReturnMessage()
        {
            var activityDto = ObjectTestUtils.BuildActivityDto();
            activityDto.Id= Guid.NewGuid();
            var activity = ObjectTestUtils.BuildActivity(Guid.NewGuid());
            _repository.Setup(repo => repo.FindByTitle(activityDto.Title)).ReturnsAsync(new List<Activity>{activity});

            IEnumerable<ValidationMessage> messages= await _validation.Validate(activityDto);
            Assert.Collection(messages,
                item => Assert.Equal("Title already exists in db", item.Message));
        }
        
        [Fact]
        public async void WhenExistingNoTitleFoundThenReturnMessageIsEmpty()
        {
            var activityDto = ObjectTestUtils.BuildActivityDto();
            activityDto.Id= Guid.NewGuid();
            IEnumerable<ValidationMessage> messages= await _validation.Validate(activityDto);
            Assert.Empty(messages);
        }
    }
}