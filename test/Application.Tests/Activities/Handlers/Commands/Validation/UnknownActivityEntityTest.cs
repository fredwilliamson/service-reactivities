﻿using System;
using System.Collections.Generic;
using Application.Activities.Commands.Handlers.Validation;
using Application.Commands;
using Moq;
using Persistence.Repositories;
using Xunit;

namespace Application.Tests.Activities.Handlers.Commands.Validation
{
    public class UnknownActivityEntityTest
    {
        private readonly UnknownActivityEntity _validation;
        
        private readonly Mock<IActivityRepository> _repository;

        public UnknownActivityEntityTest()
        {
            _repository = new Mock<IActivityRepository>();
            _validation = new UnknownActivityEntity(_repository.Object);
        }

        [Fact]
        public async void WhenUnknownActivityFoundThenReturnMessage()
        {
            var activityDto = ObjectTestUtils.BuildActivityDto();
            activityDto.Id= Guid.NewGuid();
            var id = activityDto.Id;
            
            IEnumerable<ValidationMessage> messages= await _validation.Validate(activityDto);
            Assert.Collection(messages,
                item => Assert.Equal($"activity with id {id} not found", item.Message));
        }
        
        [Fact]
        public async void WhenKnownActivityFoundThenReturnMessageIsEmpty()
        {
            var activityDto = ObjectTestUtils.BuildActivityDto();
            activityDto.Id= Guid.NewGuid();
          
            var activity = ObjectTestUtils.BuildActivity(activityDto.Id);
            _repository.Setup(repo => repo.FindEntityById(activityDto.Id)).ReturnsAsync(activity);
            
            IEnumerable<ValidationMessage> messages= await _validation.Validate(activityDto);
            Assert.Empty(messages);
        }
    }
}