using System;
using System.Collections.Generic;
using Application.Activities.Commands;
using Application.Commands;
using Domain.Dtos;
using Domain.Models;

namespace Application.Tests
{
    public static class ObjectTestUtils
    {
        public static ActivityDto BuildActivityDto()
        {
            return new ActivityDto {Title = "FME TEST", Category = "TEST", City = "Nantes"};
        }
        public static Activity BuildActivity(Guid id)
        {
            return new Activity {Id = id,Title = "FME TEST", Category = "TEST", City = "Nantes"};
        }
        public static IEnumerable<ValidationMessage> BuilValidationMessages()
        {
            return new List<ValidationMessage>() {ValidationMessage.ToError("Existing title")};;
        }

        public static User BuildUser()
        {
            return new User
            {
                Id = Guid.NewGuid().ToString(),
                UserName = "Bob",
                Email = "Bob@test.com"
            };
        }
    }
}
